import argparse
import glob
import json
import os
import time

import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa
from dotmap import DotMap

from training.dataset import get_filepaths_per_class


def process_input_arguments() -> DotMap:
    ap = argparse.ArgumentParser()

    ap.add_argument("--input_images_dir", type=str, required=True, help="Path to input images directory.")
    ap.add_argument("--output_dir", type=str, required=True,
                    help="Path to output directory, where the images will be saved.")
    ap.add_argument("--num_images", type=int, required=True, help="Num images which will be created for each class.")
    ap.add_argument("--batch_size", type=int, default=32,
                    help="Num images which will be processed at one time.")
    ap.add_argument("--target_img_width", type=int, default=224, help="Target image width.")
    ap.add_argument("--target_img_height", type=int, default=224, help="Target image height.")

    params = DotMap(vars(ap.parse_args()))
    print('Input params:')
    print(json.dumps(params, indent=2))

    return params


@tf.function
def process_images(images, target_width, target_height):
    random_central_fraction = np.random.uniform(0.8, 1)
    images = tf.image.central_crop(images, central_fraction=random_central_fraction)

    if np.random.uniform(0, 1) >= 0.5:
        random_angle = np.random.choice([(1 / 2) * np.pi, np.pi, (27 / 18) * np.pi])  # 90, 180, 270 degrees
        random_angle = tf.cast(tf.constant(random_angle), dtype=tf.float32)
        images = tfa.image.rotate(images, random_angle)

    if np.random.uniform(0, 1) >= 0.5:
        images = tf.image.flip_up_down(images)

    if np.random.uniform(0, 1) >= 0.5:
        images = tf.image.flip_left_right(images)

    if np.random.uniform(0, 1) >= 0.5:
        images = tf.image.random_brightness(images, max_delta=0.1)

    if np.random.uniform(0, 1) >= 0.5:
        images = tf.image.random_saturation(images, lower=0.9, upper=1.1)

    images = tf.image.resize_with_pad(images, target_width=target_width, target_height=target_height)
    return images


def augment(input_images_dir, output_dir, num_images, batch_size, target_img_width, target_img_height):
    all_filepaths = glob.glob(os.path.join(input_images_dir, '*'))
    filepaths_per_class = get_filepaths_per_class(all_filepaths)

    filepaths_for_processing = []
    for clazz, filepaths in filepaths_per_class.items():
        replace = True if len(filepaths) < num_images else False
        filepaths_for_processing.extend(np.random.choice(filepaths, size=num_images, replace=replace))

    filepaths_batches = [filepaths_for_processing[i:i + batch_size] for i in
                         range(0, len(filepaths_for_processing), batch_size)]

    counter = 0
    for filepaths_batch in filepaths_batches:
        images = []
        labels = []

        for filepath in filepaths_batch:
            image = tf.io.read_file(filepath)
            image = tf.image.decode_image(image, channels=3, dtype=tf.float32)
            images.append(image)
            labels.append(os.path.basename(filepath).split('_')[0])
        images = process_images(tf.convert_to_tensor(images),
                                tf.constant(target_img_width), tf.constant(target_img_height))

        for image, label in zip(images, labels):
            filepath = os.path.join(output_dir, '{}_aug_img_{}.jpg'.format(label, counter))
            tf.keras.preprocessing.image.save_img(filepath, image)
            counter = counter + 1


def main():
    params = process_input_arguments()

    os.makedirs(params.output_dir, exist_ok=True)

    start_time = time.time()
    augment(
        input_images_dir=params.input_images_dir,
        output_dir=params.output_dir,
        num_images=params.num_images,
        batch_size=params.batch_size,
        target_img_width=params.target_img_width,
        target_img_height=params.target_img_height
    )

    print('Result images are now in folder {}.'.format(params.output_dir))
    print('Images was created after {} sec.'.format(time.time() - start_time))


if __name__ == '__main__':
    main()
