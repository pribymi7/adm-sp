# Semestrální práce MI-ADM

Úkolem této semestrální práce je z daných dermoskopických obrázků rozpoznat o jaké se jedná nádorové onemocnění (klasifikační problém s 9 třídami). Problém vychází ze [soutěže ISIC 2019](https://challenge2019.isic-archive.com/) .

## Report
   
* [výsledný report v pdf](./report/report.pdf)

## Dataset

* zdroj dat
	* https://challenge2019.isic-archive.com/data.html
* datset obsahuje
    * obrázky
	* labels
	* metadata
* předzpracování datasetu do požadované struktury:

```bash
python dataset_utils.py create --images_dir ./mini_dataset_100/train/images/ --output ./my_dataset/ --images_labels_path ./mini_dataset_100/train/train_df.csv
python dataset_utils.py copy_images --images_dir ../stylegan2-ada/traininin-runs/00006-256_DF_dataset-auto1/out/ --label 5 --output ./my_dataset/train/images/
```

* předzpracování obrázků:

```bash
python preprocess_images.py --images_dir ./my_dataset/train/images/ --target_image_resolution 224 --output ./my_preprocess_dataset/train/images/
python preprocess_images.py --images_dir ./my_dataset/val/images/ --target_image_resolution 224 --output ./my_preprocess_dataset/val/images/
```
	
## Trénování

* spustit trénování:

```bash
python train.py --train_images_dir ./my_preprocess_dataset/train/images/ --val_images_dir ./my_preprocess_dataset/val/images/ --train_batch_size 5 --val_batch_size 1 --epochs 2
```

* shrnutí:

```bash
python train_summary.py --train_metrics_loss_path ./experiments/new_exp/run_58/train_metrics_and_loss.csv --val_metrics_loss_path ./experiments/new_exp/run_58/val_metrics_and_loss.csv --output_dir ./experiments/new_exp/run_58/
```


