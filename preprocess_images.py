import argparse
import json
import os

import numpy as np
import tensorflow as tf
from dotmap import DotMap
from tensorflow.python.framework import ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import image_ops
from tensorflow.python.ops import math_ops


def _str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('True', 'true'):
        return True
    elif v.lower() in ('False', 'false'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def process_input_arguments() -> DotMap:
    ap = argparse.ArgumentParser()

    ap.add_argument("--images_dir", type=str, required=True, help="Path to images")
    ap.add_argument("--output", type=str, default="./output/", help="Path to result images")
    ap.add_argument("--with_color_constancy", type=_str2bool, default=False, help="Do color constancy on images.")
    ap.add_argument("--target_image_resolution", type=int, default=224,
                    help="Image resolution of input images. Width and height of images will be same.")

    params = DotMap(vars(ap.parse_args()))
    print('Input params:')
    print(json.dumps(params, indent=2))
    return params


def color_constancy(img):
    def shades_gray(image, njet=0, mink_norm=1, sigma=1):
        # copied from https://github.com/anindox8/Ensemble-of-Multi-Scale-CNN-for-Dermatoscopy-Classification/blob/master/scripts/color-io.ipynb
        """
        Estimates the light source of an input_image as proposed in:
        J. van de Weijer, Th. Gevers, A. Gijsenij
        "Edge-Based Color Constancy"
        IEEE Trans. Image Processing, accepted 2007.
        Depending on the parameters the estimation is equal to Grey-World, Max-RGB, general Grey-World,
        Shades-of-Grey or Grey-Edge algorithm.
        :param image: rgb input image (NxMx3)
        :param njet: the order of differentiation (range from 0-2)
        :param mink_norm: minkowski norm used (if mink_norm==-1 then the max
               operation is applied which is equal to minkowski_norm=infinity).
        :param sigma: sigma used for gaussian pre-processing of input image
        :return: illuminant color estimation
        :raise: ValueError
        Ref: https://github.com/MinaSGorgy/Color-Constancy
        """
        from skimage import filters

        gauss_image = filters.gaussian(image, sigma=sigma, multichannel=True)
        if njet == 0:
            deriv_image = [gauss_image[:, :, channel] for channel in range(3)]
        else:
            if njet == 1:
                deriv_filter = filters.sobel
            elif njet == 2:
                deriv_filter = filters.laplace
            else:
                raise ValueError("njet should be in range[0-2]! Given value is: " + str(njet))
            deriv_image = [np.abs(deriv_filter(gauss_image[:, :, channel])) for channel in range(3)]
        for channel in range(3):
            deriv_image[channel][image[:, :, channel] >= 255] = 0.
        if mink_norm == -1:
            estimating_func = np.max
        else:
            estimating_func = lambda x: np.power(np.sum(np.power(x, mink_norm)), 1 / mink_norm)
        illum = [estimating_func(channel) for channel in deriv_image]
        som = np.sqrt(np.sum(np.power(illum, 2)))
        illum = np.divide(illum, som)
        return illum

    def correct_image(image, illum):
        # copied from https://github.com/anindox8/Ensemble-of-Multi-Scale-CNN-for-Dermatoscopy-Classification/blob/master/scripts/color-io.ipynb
        """
        Corrects image colors by performing diagonal transformation according to
        given estimated illumination of the image.
        :param image: rgb input image (NxMx3)
        :param illum: estimated illumination of the image
        :return: corrected image
        Ref: https://github.com/MinaSGorgy/Color-Constancy
        """

        correcting_illum = illum * np.sqrt(3)
        corrected_image = image / 255.
        for channel in range(3):
            corrected_image[:, :, channel] /= correcting_illum[channel]
        return np.clip(corrected_image, 0., 1.)

    gw = correct_image(img, shades_gray(img, njet=0, mink_norm=+6, sigma=0))  # Gray World Constancy
    return gw * 255


def smart_resize(x, size, interpolation='bilinear'):
    # copied from https://github.com/yixingfu/tensorflow/blob/d94980b6923810f91ab6d2de2c40c59d29001fc9/tensorflow/python/keras/preprocessing/image.py
    # from TensorFlow 2.4.0-rc1
    if len(size) != 2:
        raise ValueError('Expected `size` to be a tuple of 2 integers, '
                         'but got: %s' % (size,))
    img = ops.convert_to_tensor(x)
    if img.shape.rank is not None:
        if img.shape.rank != 3:
            raise ValueError(
                'Expected an image array with shape `(height, width, channels)`, but '
                'got input with incorrect rank, of shape %s' % (img.shape,))
    shape = array_ops.shape(img)
    height, width = shape[0], shape[1]
    target_height, target_width = size

    crop_height = math_ops.cast(
        math_ops.cast(width * target_height, 'float32') / target_width,
        'int32')
    crop_width = math_ops.cast(
        math_ops.cast(height * target_width, 'float32') / target_height,
        'int32')

    # Set back to input height / width if crop_height / crop_width is not smaller.
    crop_height = math_ops.minimum(height, crop_height)
    crop_width = math_ops.minimum(width, crop_width)

    crop_box_hstart = math_ops.cast(
        math_ops.cast(height - crop_height, 'float32') / 2, 'int32')
    crop_box_wstart = math_ops.cast(
        math_ops.cast(width - crop_width, 'float32') / 2, 'int32')

    crop_box_start = array_ops.stack([crop_box_hstart, crop_box_wstart, 0])
    crop_box_size = array_ops.stack([crop_height, crop_width, -1])
    crop_box_start = array_ops.stack(crop_box_start)
    crop_box_size = array_ops.stack(crop_box_size)

    img = array_ops.slice(img, crop_box_start, crop_box_size)
    img = image_ops.resize_images_v2(
        images=img,
        size=size,
        method=interpolation)
    if isinstance(x, np.ndarray):
        return img.numpy()
    return img


def main():
    params = process_input_arguments()

    os.makedirs(params.output, exist_ok=True)

    ds = tf.data.Dataset.list_files(os.path.join(params.images_dir, '*'))

    for image_path in ds:
        image = tf.io.read_file(image_path)
        image = tf.image.decode_image(image, channels=3, dtype=tf.uint8)
        if params.with_color_constancy:
            image = tf.numpy_function(color_constancy, [image], tf.float32)
        image = smart_resize(image, (params.target_image_resolution, params.target_image_resolution),
                             interpolation='bicubic')

        image_name = os.path.basename(image_path.numpy().decode('utf-8'))
        tf.keras.preprocessing.image.save_img(os.path.join(params.output, image_name), image)


if __name__ == '__main__':
    main()
