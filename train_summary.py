import argparse
import json
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from dotmap import DotMap


def process_input_arguments() -> DotMap:
    ap = argparse.ArgumentParser()

    ap.add_argument("--train_metrics_loss_path", type=str, required=True, help="Path to train metrics and loss file.")
    ap.add_argument("--val_metrics_loss_path", type=str, required=True, help="Path to val metrics and loss file.")
    ap.add_argument("--output_dir", type=str, required=True, help="Output directory.")
    ap.add_argument("--all_to_one", type=bool, default=True,
                    help="All metrics and loss progress save to one image if arg is True. Otherwise save to separate images.")

    params = DotMap(vars(ap.parse_args()))
    print('Input params:')
    print(json.dumps(params, indent=2))
    return params


def save_best_values(cols, train_progress_df, val_progress_df, path):
    best_values = DotMap()
    for col in cols:
        best_values[col].train.value = float(train_progress_df[col].min()) if col is 'loss' else float(
            train_progress_df[col].max())
        best_values[col].train.epoch = int(train_progress_df[col].argmin() + 1) if col is 'loss' else int(
            train_progress_df[col].argmax() + 1)

        best_values[col].val.value = float(val_progress_df[col].min()) if col is 'loss' else float(
            val_progress_df[col].max())
        best_values[col].val.epoch = int(val_progress_df[col].argmin() + 1) if col is 'loss' else int(
            val_progress_df[col].argmax() + 1)

    with open(path, 'w') as f:
        json.dump(best_values, f, indent=2)


def _plot_state(ax, datas, labels, title, xlabel, ylabel):
    assert len(datas) == len(labels), 'size of arrays datas and labels must be same'
    for data, label in zip(datas, labels):
        ax.plot(data, label=label)
    ax.set_title(title)
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.legend()


def _set_first_letter_upper(in_str):
    return in_str[0].upper() + in_str[1:]


def _get_title(in_str):
    return _set_first_letter_upper(in_str).replace('_', ' ')


def _get_ylabel(in_str):
    return 'loss' if in_str == 'loss' else 'metric'


def save_all_in_one_figure(cols, train_df, val_df, path):
    nrows = int(np.ceil(len(cols) / 2))
    fig, axs = plt.subplots(nrows, 2, squeeze=False, figsize=(10, 20))

    for row_pos in range(nrows):
        for col_pos in range(2):
            ix = col_pos + (row_pos * 2)
            if ix <= (len(cols) - 1):
                cur_col = cols[ix]
                _plot_state(axs[row_pos, col_pos],
                            datas=[train_df[cur_col], val_df[cur_col]],
                            labels=['train', 'val'], title=_get_title(cur_col),
                            xlabel='epoch', ylabel=_get_ylabel(cur_col))
            else:
                axs[row_pos, 1].axis('off')

    plt.tight_layout()
    plt.savefig(path)


def save_all_in_separate_figures(cols, train_df, val_df, output_dir):
    for cur_col in cols:
        fig, axs = plt.subplots(1, squeeze=True, figsize=(6, 4))
        _plot_state(axs,
                    datas=[train_df[cur_col], val_df[cur_col]],
                    labels=['train', 'val'], title=_get_title(cur_col),
                    xlabel='epoch', ylabel=_get_ylabel(cur_col))
        plt.tight_layout()
        plt.savefig(os.path.join(output_dir, cur_col + '.pdf'))
        plt.close(fig)


def main():
    params = process_input_arguments()

    train_progress_df = pd.read_csv(params.train_metrics_loss_path)
    val_progress_df = pd.read_csv(params.val_metrics_loss_path)
    cols = train_progress_df.columns

    path = os.path.join(params.output_dir, 'best_values.json')
    save_best_values(cols, train_progress_df, val_progress_df, path)
    print('Best values was saved to: ' + path)

    if params.all_to_one:
        path = os.path.join(params.output_dir, 'summary.pdf')
        save_all_in_one_figure(cols, train_progress_df, val_progress_df, path)
    else:
        save_all_in_separate_figures(cols, train_progress_df, val_progress_df, params.output_dir)
    print('Training progress was saved to: ' + params.output_dir)


if __name__ == '__main__':
    main()
