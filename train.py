import argparse
import json
import os

from dotmap import DotMap
from tensorflow.keras.callbacks import ModelCheckpoint, LambdaCallback, LearningRateScheduler
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.optimizers import Adam

from training.callback import create_callback_log_metrics_and_loss, create_callback_log_training_progress
from training.dataset import get_y, get_filenames, LABELS, calc_steps_per_epoch, get_weights_dict, \
    create_training_datasets
from training.experiment import init_experiment
from training.lr_scheduler import create_decay_schedule
from training.metric import get_metrics_and_their_names
from training.model import create_model, PRE_TRAINED_MODEL_PARAMS
from tensorflow.keras.applications import EfficientNetB0, EfficientNetB1, EfficientNetB2, EfficientNetB3


def experiments_loop(
        n_exp,
        exp_name,
        exp_dir,
        labels,
        opt_fun,
        loss_fun,
        train_ds,
        val_ds,
        train_steps_per_epoch,
        val_steps_per_epoch,
        classes_weights,
        lr_decay_factor,
        lr_drop_every,
        epochs,
        init_epoch,
        printable_params,
        pretrained_model_cl
):
    for cur_exp in range(n_exp):
        metrics_fun, metrics_names = get_metrics_and_their_names(labels)
        model = create_model(
            metrics=[metrics_fun],
            optimizer=opt_fun,
            loss=loss_fun,
            pretrained_model=pretrained_model_cl
        )

        metrics_names = metrics_names
        path_to_run = init_experiment(exp_dir, exp_name, printable_params, metrics_names)

        model.fit(
            train_ds,
            epochs=epochs,
            steps_per_epoch=train_steps_per_epoch,
            validation_data=val_ds,
            validation_steps=val_steps_per_epoch,
            class_weight=classes_weights,
            callbacks=[
                ModelCheckpoint(
                    filepath=os.path.join(path_to_run, 'saved_models/model_{epoch:d}.hdf5'),
                    save_weights_only=True
                ),
                LambdaCallback(
                    on_epoch_end=create_callback_log_metrics_and_loss(path_to_run, metrics_names + ['loss'])),
                LambdaCallback(on_epoch_end=create_callback_log_training_progress()),
                LearningRateScheduler(create_decay_schedule(lr_decay_factor, lr_drop_every))
            ],
            initial_epoch=init_epoch,
            verbose=0
        )


def _save_training_params(path, params):
    with open(path, 'w') as f:
        json.dump(params, f, indent=2)


def _check_valid_number(value):
    try:
        float(value)
    except ValueError:
        raise argparse.ArgumentTypeError("%s is an invalid number" % value)
    else:
        return float(value)


def process_training_input_arguments() -> DotMap:
    ap = argparse.ArgumentParser()

    group = ap.add_argument_group('dataset options')
    group.add_argument("--train_images_dir", type=str, required=True, help="Path to train images")
    group.add_argument("--val_images_dir", type=str, required=True, help="Path to val images")
    group.add_argument("--image_resolution", type=int, default=224,
                       help="Image resolution of input images. Width and height of images must be same.")
    group.add_argument("--dataset_stats_mean", nargs="+", type=_check_valid_number, default=[0.485, 0.456, 0.406],
                       help="Mean statistic of dataset")
    group.add_argument("--dataset_stats_std", nargs="+", type=_check_valid_number, default=[0.229, 0.224, 0.225],
                       help="Std statistic of dataset")
    group.add_argument("--n_channels", type=int, default=3, help="Num channels of images")

    group = ap.add_argument_group('training options')
    group.add_argument("--train_batch_size", type=int, default=10, help="size of training batch size")
    group.add_argument("--val_batch_size", type=int, default=10, help="size of validation batch size")
    group.add_argument("--lr", type=float, default=0.001, help="Learning rate of optimizer")
    group.add_argument("--opt", type=str, default='adam', choices=['adam'], help="Optimizer")
    group.add_argument("--loss", type=str, default='categorical_crossentropy', choices=['categorical_crossentropy'],
                       help="Loss function")
    group.add_argument("--init_epoch", type=int, default=0, help="Init epoch")
    group.add_argument("--epochs", type=int, default=1, help="How much epochs train the model.")
    group.add_argument("--pretrained_model", type=str, default='en_b0',
                       choices=['en_b0', 'en_b1', 'en_b2', 'en_b3', 'dn'],
                       help="How much epochs train the model.")

    group = ap.add_argument_group('learning rate schedulers options')
    group.add_argument("--lr_decay_factor", type=float, default=0.5, help="Decay factor")
    group.add_argument("--lr_drop_every", type=int, default=10, help="Drop lr every x-th epochs.")

    group = ap.add_argument_group('experiment options')
    group.add_argument("--exp_dir", type=str, default="./experiments", help="Experiment directory")
    group.add_argument("--exp_name", type=str, default="new_exp", help="Experiment name")
    group.add_argument("--n_exp", type=int, default=1, help="Number of experiments.")

    return DotMap(vars(ap.parse_args()))


def get_printable_params(params):
    excludes_keys_from_print = ['opt_fun', 'loss_fun', 'train_one_hot_labels', 'val_one_hot_labels',
                                'pretrained_model_cl']
    params_for_print = dict(filter(lambda elem: elem[0] not in excludes_keys_from_print, params.items()))
    return params_for_print


def create_training_params(input_args) -> DotMap:
    params = DotMap(input_args)

    train_filenames = get_filenames(params.train_images_dir)
    params.labels = LABELS
    params.n_labels = len(params.labels)

    params.classes_weights = get_weights_dict(get_y(train_filenames))

    params.train_steps_per_epoch = calc_steps_per_epoch(len(train_filenames), params.train_batch_size)
    val_filenames = get_filenames(params.val_images_dir)
    params.val_steps_per_epoch = calc_steps_per_epoch(len(val_filenames), params.val_batch_size)

    if params.opt == 'adam':
        params.opt_fun = Adam(lr=params.lr)
    if params.loss == 'categorical_crossentropy':
        params.loss_fun = CategoricalCrossentropy(name='categorical_crossentropy')

    pre_trained_model_params = dict(PRE_TRAINED_MODEL_PARAMS)
    pre_trained_model_params['input_shape'] = (params.image_resolution, params.image_resolution, params.n_channels)
    if params.pretrained_model == 'en_b0':
        params.pretrained_model_cl = EfficientNetB0(**pre_trained_model_params)
    if params.pretrained_model == 'en_b1':
        params.pretrained_model_cl = EfficientNetB1(**pre_trained_model_params)
    if params.pretrained_model == 'en_b2':
        params.pretrained_model_cl = EfficientNetB2(**pre_trained_model_params)
    if params.pretrained_model == 'en_b3':
        params.pretrained_model_cl = EfficientNetB3(**pre_trained_model_params)
    # if params.pretrained_model is 'dn':
    #     params.pretrained_model_cl = DenseNet(**pre_trained_model_params)

    params_for_print = get_printable_params(params)
    print('Training params:')
    print(json.dumps(params_for_print, indent=2))

    return params


def main():
    input_args = process_training_input_arguments()
    params = create_training_params(input_args)

    train_ds, val_ds = create_training_datasets(
        params.train_batch_size,
        params.train_images_dir,
        params.val_batch_size,
        params.val_images_dir,
        params.image_resolution
    )

    experiments_loop(
        n_exp=params.n_exp,
        exp_name=params.exp_name,
        exp_dir=params.exp_dir,
        labels=params.labels,
        opt_fun=params.opt_fun,
        loss_fun=params.loss_fun,
        train_ds=train_ds,
        val_ds=val_ds,
        train_steps_per_epoch=params.train_steps_per_epoch,
        val_steps_per_epoch=params.val_steps_per_epoch,
        classes_weights=params.classes_weights,
        lr_decay_factor=params.lr_decay_factor,
        lr_drop_every=params.lr_drop_every,
        epochs=params.epochs,
        init_epoch=params.init_epoch,
        printable_params=get_printable_params(params),
        pretrained_model_cl=params.pretrained_model_cl
    )


if __name__ == "__main__":
    main()
