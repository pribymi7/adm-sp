import argparse
import json
import os
import shutil

import pandas as pd
import tensorflow as tf
from dotmap import DotMap
from sklearn.model_selection import train_test_split


def create_dataset(filenames):
    x = []
    y = []

    for filename in filenames:
        label = filename.split('_')[0]
        x.append(filename)
        y.append(label)

    return x, y


def get_filenames(dirname):
    filenames = []
    for filename in os.listdir(dirname):
        if os.path.isfile(os.path.join(dirname, filename)):
            filenames.append(filename)
    return filenames


def copy_images(images_dir, output):
    filenames = get_filenames(images_dir)
    for filename in filenames:
        shutil.copy(os.path.join(images_dir, filename), os.path.join(output, filename))


def create(images_dir, images_labels_path, train_size, output):
    copy_images_and_add_labels(output, images_labels_path, images_dir)
    create_train_val_split(output, output, train_size)


def create_train_val_split(images_dir, output, train_size):
    filenames = get_filenames(images_dir)
    os.makedirs(os.path.join(output, 'train/images'), exist_ok=True)
    os.makedirs(os.path.join(output, 'val/images'), exist_ok=True)

    x, y = create_dataset(filenames)

    x_train, x_val, y_train, y_val = train_test_split(x, y, stratify=y, train_size=train_size)

    for filename in x_train:
        shutil.move(os.path.join(images_dir, filename), os.path.join(output, 'train/images/', filename))
    for filename in x_val:
        shutil.move(os.path.join(images_dir, filename), os.path.join(output, 'val/images/', filename))


def copy_images_and_add_labels(output, images_labels_path, images_dir):
    copy_images(images_dir, output)
    add_labels(images_labels_path, images_dir)


def add_labels(images_labels_path, images_dir):
    images_labels_df = pd.read_csv(images_labels_path)
    images_names = images_labels_df['image'] + '.jpg'
    labels = images_labels_df.iloc[:, 1:]
    ds = tf.data.Dataset.from_tensor_slices((images_names, labels))

    for image_name, label in ds:
        from_filepath = os.path.join(images_dir, str(image_name.numpy().decode('utf-8')))
        label_str = str(tf.argmax(label, axis=0).numpy())
        to_filename =  label_str + '_' + str(image_name.numpy().decode('utf-8'))
        to_filepath = os.path.join(images_dir, to_filename)
        tf.io.gfile.rename(from_filepath, to_filepath, overwrite=True)


def process_input_arguments_and_execute_command() -> DotMap:
    ap = argparse.ArgumentParser()
    subparsers = ap.add_subparsers(help="commands", dest="command", required=True)

    parser_copy_images = subparsers.add_parser('copy_images', help="Copy images to new location.")
    parser_copy_images.add_argument("--images_dir", type=str, required=True, help="Path to images")
    parser_copy_images.add_argument("--output", type=str, default="./output/", help="Destination of copied images.")
    parser_copy_images.set_defaults(func=copy_images)

    parser_add_labels = subparsers.add_parser('add_labels',
                                              help="Add labels to image names. Rename image img1.jpg to label_img1.jpg")
    parser_add_labels.add_argument("--images_dir", type=str, required=True, help="Path to images")
    parser_add_labels.add_argument("--images_labels_path", type=str,
                                   help="Path to csv file with image names and labels")
    parser_add_labels.set_defaults(func=add_labels)

    parser_copy_images_and_add_labels = subparsers.add_parser('copy_images_and_add_labels',
                                                              help='Structure dataset.')
    parser_copy_images_and_add_labels.add_argument("--images_dir", type=str, required=True, help="Path to images")
    parser_copy_images_and_add_labels.add_argument("--images_labels_path", type=str,
                                                   help="Path to csv file with image names and labels")
    parser_copy_images_and_add_labels.add_argument("--output", type=str, default="./output/",
                                                   help="Path to new dataset")
    parser_copy_images_and_add_labels.set_defaults(func=copy_images_and_add_labels)

    parser_split = subparsers.add_parser('train_val_split', help='Do train val split to folders train and val.')
    parser_split.add_argument("--images_dir", type=str, required=True, help="Path to images")
    parser_split.add_argument("--train_size", type=float, default=0.8,
                              help="Represent the proportion of the dataset to include in the train split")
    parser_split.add_argument("--output", type=str, default="./output/", help="Path to new split dataset")
    parser_split.set_defaults(func=create_train_val_split)

    parser_create = subparsers.add_parser('create', help='Execute commands structure and train_val_split.')
    parser_create.add_argument("--images_dir", type=str, required=True, help="Path to images")
    parser_create.add_argument("--images_labels_path", type=str,
                               help="Path to csv file with images names and labels")
    parser_create.add_argument("--train_size", type=float, default=0.8,
                               help="Represent the proportion of the dataset to include in the train split")
    parser_create.add_argument("--output", type=str, default="./output/", help="Path to new dataset")
    parser_create.set_defaults(func=create)

    args = ap.parse_args()
    params = DotMap(vars(args))
    del params['func']
    print('Input params:')
    print(json.dumps(params, indent=2))

    del params['command']
    args.func(**params)

    return params


def main():
    process_input_arguments_and_execute_command()


if __name__ == '__main__':
    main()
