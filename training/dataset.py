import os

import numpy as np
import tensorflow as tf

LABELS = ['MEL', 'NV', 'BCC', 'AK', 'BKL', 'DF', 'VASC', 'SCC']

LABEL_MAPPER = {
    0: 'MEL',
    1: 'NV',
    2: 'BCC',
    3: 'AK',
    4: 'BKL',
    5: 'DF',
    6: 'VASC',
    7: 'SCC'
}


def calc_steps_per_epoch(n, batch_size):
    return np.ceil(n / batch_size)


def get_filenames(images_dir):
    filenames = []
    for filename in os.listdir(images_dir):
        if os.path.isfile(os.path.join(images_dir, filename)):
            filenames.append(filename)
    return filenames


def get_filepaths_per_class(filepaths):
    filepaths_per_class = {clazz: [] for clazz in range(len(LABELS))}
    for filepath in filepaths:
        clazz = int(os.path.basename(filepath).split('_')[0])
        filepaths_per_class[clazz].append(filepath)
    return filepaths_per_class


def get_y(filenames):
    y = []
    for filename in filenames:
        y.append(int(filename.split('_')[0]))
    return y


def get_weights_dict(y):
    weights = {i: 1 for i in range(len(LABELS))}
    weights.update({label: 1 / y.count(label) for label in y})
    return weights


class ImageDataSetCreator:
    def __init__(self, batch_size, image_height, image_width, images_dir):
        self.batch_size = batch_size
        self.image_height = image_height
        self.image_width = image_width
        self.images_dir = os.path.normpath(images_dir)

    @tf.function
    def read_image(self, image_path):
        image = tf.io.read_file(self.images_dir + os.sep + image_path)
        image = tf.image.decode_image(image, channels=3, dtype=tf.float32)
        label = tf.strings.to_number(tf.strings.split(image_path, sep='_')[0], out_type=tf.int64)
        return image, tf.one_hot(label, depth=len(LABELS))

    @tf.function
    def normalize(self, image):
        return tf.image.per_image_standardization(image)

    @tf.function
    def preprocess(self, image_path):
        image, label = self.read_image(image_path)
        image = self.normalize(image)
        return image, label


def create_image_dataset_creator(batch_size, images_dir, image_resolution):
    return ImageDataSetCreator(
        batch_size=batch_size,
        images_dir=images_dir,
        image_width=image_resolution,
        image_height=image_resolution
    )


def create_dataset_from_tensor_slices(images_names, batch_size, image_ds_creator):
    ds = tf.data.Dataset.from_tensor_slices(images_names)
    ds = ds.map(image_ds_creator.preprocess, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    ds = ds.shuffle(buffer_size=2048)
    ds = ds.batch(batch_size=batch_size)
    ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return ds


def create_training_datasets(train_batch_size, train_images_dir, val_batch_size, val_images_dir, image_resolution):
    train_image_ds_creator = create_image_dataset_creator(
        train_batch_size,
        train_images_dir,
        image_resolution
    )
    train_ds = create_dataset_from_tensor_slices(
        get_filenames(train_images_dir),
        train_batch_size,
        train_image_ds_creator
    )

    val_image_ds_creator = create_image_dataset_creator(
        val_batch_size,
        val_images_dir,
        image_resolution
    )
    val_ds = create_dataset_from_tensor_slices(
        get_filenames(val_images_dir),
        val_batch_size,
        val_image_ds_creator
    )

    return train_ds, val_ds
