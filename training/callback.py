import os

import pandas as pd


def create_callback_log_metrics_and_loss(path_to_run, metrics_and_loss_names):
    def log_metrics_and_loss(epoch, logs):
        save_data(logs, metrics_and_loss_names, path_to_run, 'train')
        save_data(logs, metrics_and_loss_names, path_to_run, 'val')

    def save_data(logs, metrics_and_loss_names, path_to_run, file_prefix):
        prefix = 'val_' if file_prefix == 'val' else ''
        data_df = pd.DataFrame([[logs[prefix + m] for m in metrics_and_loss_names]], columns=metrics_and_loss_names)

        old_df = pd.read_csv(os.path.join(path_to_run, file_prefix + '_metrics_and_loss.csv'))
        new_df = old_df.append(data_df)
        new_df.to_csv(os.path.join(path_to_run, file_prefix + '_metrics_and_loss.csv'), index=None, header=True)

    return log_metrics_and_loss


def create_callback_log_training_progress():
    def log_training_progress(epoch, logs):
        print("epoch:", epoch + 1,
              "train_loss:", "{:.3f}".format(logs['loss']),
              "val_loss:", "{:.3f}".format(logs['val_loss']),
              "train_acc:", "{:.2f}".format(logs['accuracy']),
              "val_acc:", "{:.2f}".format(logs['val_accuracy']),
              "train_AUC:", "{:.2f}".format(logs['AUC']),
              "val_AUC:", "{:.2f}".format(logs['val_AUC']),
              "train_mean_sensitivity:", "{:.2f}".format(logs['mean_sensitivity']),
              "val_mean_sensitivity:", "{:.2f}".format(logs['val_mean_sensitivity']),
              )

    return log_training_progress
