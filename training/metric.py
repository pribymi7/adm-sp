import tensorflow as tf
from tensorflow.keras.metrics import AUC, CategoricalAccuracy, Metric, Recall


class MeanSensitivityMetric(Metric):

    def __init__(self, labels, **kwargs):
        super(MeanSensitivityMetric, self).__init__(name='mean_sensitivity', **kwargs)
        self.labels = labels

    def reset_states(self):
        return

    def update_state(self, y_true, y_pred, sample_weight=None):
        return

    def result(self):
        return 1

    def fill_output(self, output):
        result = tf.constant(0, dtype=tf.float32)
        for label in self.labels:
            key = 'sensitivity_' + label
            result = tf.add(result, output[key])
        output['mean_sensitivity'] = tf.divide(result, len(self.labels))


def create_sensitivities(labels):
    return [Recall(top_k=1, class_id=i, name='sensitivity_' + label) for i, label in enumerate(labels)]


def get_metrics_and_their_names(labels):
    sensitivity_metrics_functions = create_sensitivities(labels)
    other_metrics_functions = [AUC(name='AUC'), CategoricalAccuracy(name="accuracy"), MeanSensitivityMetric(labels)]

    all_metrics_fun = sensitivity_metrics_functions + other_metrics_functions
    all_metrics_names = [metric_fun.name for metric_fun in all_metrics_fun]

    return all_metrics_fun, all_metrics_names
