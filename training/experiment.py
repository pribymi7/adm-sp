import json
import os
from pathlib import Path

import numpy as np
import pandas as pd


def init_experiment(exp_dir, exp_name, printable_params, metrics_names):
    metrics_and_loss_names = metrics_names + ['loss']
    path_to_experiment = os.path.join(exp_dir, exp_name)
    identifications = get_identifications_of_runs(path_to_experiment)
    next_id = np.max(identifications) + 1

    path_to_run = os.path.join(path_to_experiment, 'run_' + str(next_id))
    Path(os.path.join(path_to_run, 'saved_models')).mkdir(parents=True, exist_ok=True)

    save_training_params(os.path.join(path_to_run, 'params.json'), printable_params)
    pd.DataFrame(columns=metrics_and_loss_names).to_csv(os.path.join(path_to_run, 'train_metrics_and_loss.csv'),
                                                        index=False,
                                                        header=True)
    pd.DataFrame(columns=metrics_and_loss_names).to_csv(os.path.join(path_to_run, 'val_metrics_and_loss.csv'),
                                                        index=False,
                                                        header=True)

    return path_to_run


def get_identifications_of_runs(path_to_experiment):
    if os.path.isdir(path_to_experiment):
        folders = [name for name in os.listdir(path_to_experiment) if
                   os.path.isdir(os.path.join(path_to_experiment, name))]
        identifications = [int(folder_name.split('_')[1]) for folder_name in folders]
        return identifications
    else:
        return [-1]


def save_training_params(path, params):
    with open(path, 'w') as f:
        json.dump(params, f, indent=2)
