import os

import tensorflow as tf

from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Model

PRE_TRAINED_MODEL_PARAMS = {
    'weights': 'imagenet',
    'include_top': False,
    'pooling': 'avg'
}


class MyModel(Model):

    def train_step(self, data):
        if len(data) == 3:
            x, y, sample_weight = data
        else:
            x, y = data

        with tf.GradientTape() as tape:
            y_pred = self(x, training=True)
            loss = self.compiled_loss(y, y_pred, regularization_losses=self.losses)

        # Compute gradients
        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)

        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        self.compiled_metrics.update_state(y, y_pred)
        output = {m.name: m.result() for m in self.metrics}

        if 'mean_sensitivity' in self.metrics_names:
            self.metrics[-1].fill_output(output)

        return output

    def test_step(self, data):
        x, y = data

        y_pred = self(x, training=False)
        loss = self.compiled_loss(y, y_pred, regularization_losses=self.losses)

        self.compiled_metrics.update_state(y, y_pred)
        output = {m.name: m.result() for m in self.metrics}

        if 'mean_sensitivity' in self.metrics_names:
            self.metrics[-1].fill_output(output)

        return output


def create_model(metrics, optimizer, loss, pretrained_model):
    x = pretrained_model.output
    x = Dense(256, activation="relu")(x)
    x = Dense(64, activation="relu")(x)
    predictions = Dense(8, activation='softmax')(x)
    model = MyModel(inputs=pretrained_model.input, outputs=predictions)

    model.compile(
        optimizer=optimizer,
        loss=loss,
        metrics=metrics,
        # uncomment for debugging
        # run_eagerly=True
    )

    return model


def load_model(path_to_experiment, epoch, input_shape, metrics, optimizer, loss):
    model = create_model(input_shape, metrics, optimizer, loss)
    model.load_weights(os.path.join(path_to_experiment, "saved_models/model_{:02d}.hdf5".format(epoch)))
    return model
